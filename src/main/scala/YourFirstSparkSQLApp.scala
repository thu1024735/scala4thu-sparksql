import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Created by mark on 04/06/2017.
  */
object YourFirstSparkSQLApp extends App{

  val ss = SparkSession
    .builder()
    .master("local[*]")
    .appName("Spark SQL first example")
    .getOrCreate()

  val src: DataFrame =ss.read.option("header", "true")
    .csv("dataset/small-ratings.csv")

  src.createOrReplaceTempView("ratings")

//
//  //3
//  ss.sql("SELECT movieId, avg(rating) as mean FROM ratings GROUP BY movieId ORDER BY avg(rating) DESC").show()
//
//
////  "SELECT * FROM ratings WHERE movieId=30707 AND userId=107799"
//  //4
//  ss.sql("SELECT * FROM ratings WHERE movieId=30707 AND userId=107799").show()
//
//  //5
//
  val movieSrc: DataFrame =ss.read.option("header", "true")
    .csv("dataset/movies.csv")

  movieSrc.createOrReplaceTempView("movies")
//
//  ss.sql("SELECT movieId,title FROM movies WHERE movieId=10").show()

  //6
  ss.sql("SELECT m.movieId,m.title,rating,timestamp\nFROM ratings as r JOIN movies as m WHERE r.movieId=m.movieId\nORDER BY timestamp DESC").show()

}
